/**
 * Created by chenfengjuan on 16/9/27.
 */
var bomH;
var activeTab=new Array();
var $tabs=new Array(6);
//var userName;
$tabs[0]= $("<li role='presentation' name='smAssistant' class='active smAssistant-tab'><a href='#'>小助手管理<span class='close'>x</span></a></li>");
$tabs[1]= $("<li role='presentation' name='group' class='active group-tab'><a href='#'>群管理<span class='close'>x</span></a></li>");
$tabs[2]= $("<li role='presentation' name='message' class='active message-tab'><a href='#'>消息管理<span class='close'>x</span></a></li>");
$tabs[3]= $("<li role='presentation' name='keyword' class='active keyword-tab'><a href='#'>关键字管理<span class='close'>x</span></a></li>");
$tabs[4]= $("<li role='presentation' name='assistantGroup' class='active assistantGroup-tab'><a href='#'>小助手所在群<span class='close'>x</span></a></li>");
$tabs[5]= $("<li role='presentation' name='dataOut' class='active dataOut-tab'><a href='#'>数据导出<span class='close'>x</span></a></li>");
$().ready(function () {
    $.ajax({
        type: "GET",
        url: "http://192.168.0.43:8080/HelperManage/isLogin",
        success: function (data) {
            if(data.status==-1){
              //window.location.href="login.html";
            }else{
              // $("#userName").text(data.data.username);
            }
        }
    });
    bomH=$(window).height();
    $(document.body).css("height",bomH);
    //左侧导航栏开关
    $("#parent-nav").on("click",switchNav);
    //默认tab页绑定点击事件
    $(".smAssistant-tab span").on("click",closeTab);
    //打开tab页
    $(".sidebar-child li").on("click",openTab);
    //激活tab页
    $(".nav-tabs").delegate("li","click",chooseTab);
    //群管理关键字修改绑定事件
    $("#groupTbody").delegate("a:nth-child(2)","click",modifyKey);
    //群管理 修改绑定事件
    $("#groupTbody").delegate("a:nth-child(1)","click",modifyGroup);
    //关键字修改页面修改关键字
    $("#groupKeyList").delegate("button","click",modifyGroupKey);
    //关键字修改页面"＋"的功能
    $(".key-height").delegate("button[name='add']","click",addOne);
    //关键字修改页面"－"的功能
    $(".key-height").delegate("button[name='delete']","click",deleteOne);
    //input输入验证不为空
    $("#groupKeyList").delegate("input","blur",notNull);
    $(".key-height").delegate("input","blur",notNull)
    //添加关键字
    $("#keywordAdd").on("click",addKeys);

});
//关键字修改页面"＋"的功能
function addOne() {
    $(".addBefore").before(
        "<form class='form-inline keyModify' role='form'> <input " +
        "type='text' class='form-control'  > <input " +
        "type='text' class='form-control' > <input " +
        "type='date' class='form-control' > <input " +
        "type='date' class='form-control' > <button " +
        "type='button' class='btn btn-default' name='add'>＋</button> <button " +
        "type='button' class='btn btn-default' name='delete'>－</button> </form>"
    );
}
//关键字修改页面"－"的功能
function deleteOne() {
    var eLength=$(".key-height").children("form").length;
    if(eLength>1){
        $(this).parent().remove();
    }else {
        $(".key-height").addClass("none-box");
    }
}
//群管理，关键字修改
function modifyKey() {
    $(".con-box").not(".none-box").addClass("none-box");
    $("#keyModify").removeClass("none-box");
    var groupCode=$(this).parent().parent().find("td").eq(0).text();//关键字对应的群code
    var keyGroup=$(this).parent().parent().find("td").eq(1).text();//关键字对应的群名称
    $.ajax({
        type: "POST",
        url: "http://192.168.0.43:8080/HelperManage/keyword/getKeywordByGid",
        //url:"test/groupKey.json",
        data: {"id":groupCode},
        dataType: "json",
        success: function (data) {
            $("#groupKeyList").html("");//清空groupKeyList内容
            $(".key-height").addClass("none-box");
            $(".key-height").find("input").val("");
            $.each(data.data,function (i, item) {
                $("#groupKeyList").append(
                "<form class='form-inline keyModify' role='form'><input disabled='true' type='text' class='form-control marginL-3' value="+checkNull(item.city)+
                "><input disabled='true' type='text' class='form-control marginL-3' value="+checkNull(item.keyword)+
                "><input disabled='true' type='Date' class='form-control marginL-3' value="+checkNull(item.startTime)+
                "><input disabled='true' type='Date' class='form-control marginL-3' value="+checkNull(item.endTime)+
                "><button type='button' class='btn btn-default' name="+item.id+">修改</button></form>"
                );
            });
            $("#keyGroupName").text(keyGroup);
            $("#keyGroupName").addClass(groupCode);
        }
    });
}
//关键字修改页面修改关键字
var keyContentBefore=new Array();
var keyContentAfter=new Array();
function modifyGroupKey() {
    var inputDom=$(this).parent().find("input");
    var buttonName=$(this).attr("name");
    if(inputDom.attr("disabled")=="disabled"){
        keyContentBefore[0]=inputDom.eq(0).val();
        keyContentBefore[1]=inputDom.eq(1).val();
        keyContentBefore[2]=inputDom.eq(2).val();
        keyContentBefore[3]=inputDom.eq(3).val();
        inputDom.attr("disabled",false);
    }else {
        var flag=0;//关键字信息变更标识
        keyContentAfter[0]=inputDom.eq(0).val();
        keyContentAfter[1]=inputDom.eq(1).val();
        keyContentAfter[2]=inputDom.eq(2).val();
        keyContentAfter[3]=inputDom.eq(3).val();
        for(var i=0; i<4; i++){
            if(keyContentBefore[i]!=keyContentAfter[i]){
                flag++;
            }
        }
        if((keyContentAfter[0]!="")&&(keyContentAfter[1]!="")&&(keyContentAfter[2]!="")&&(keyContentAfter[3]!="")){
            if (flag > 0) {
                //发送关键字修改后的信息
                $.ajax({
                    type: "POST",
                    url: "http://192.168.0.43:8080/HelperManage/keyword/updateKeyword",
                    //url: "test/groupKey.json",
                    data: {
                        "city": keyContentAfter[0],
                        "keyword": keyContentAfter[1],
                        "startTime": keyContentAfter[2],
                        "endTime": keyContentAfter[3],
                        "id": buttonName
                    },
                    dataType: "json",
                    success: function () {
                        inputDom.attr("disabled", true);
                        alert("关键字修改成功。");
                    }
                });
            } else {
                inputDom.attr("disabled", true);
                alert("关键字已存在。");
            }
        }else {
            alert("修改的关键字不能有空值。");
        }
    }
}
//打开关键字添加部分
function openAddKey() {
    $(".key-height").removeClass("none-box");
    $(".key-height").find("input").val("");
}
//添加所创建的关键字
function addKeys() {
    //var existed=new Array();//保存已经存在的关键字
    var groupCreateKeys=new Array();
    var formLength=$(".key-height").children("form").length;
    for(var i=0; i<formLength; i++){
        var createKey=new Object();
        createKey.key1=$(".key-height").children("form").eq(i).children("input").eq(0).val();
        createKey.key2=$(".key-height").children("form").eq(i).children("input").eq(1).val();
        createKey.key3=$(".key-height").children("form").eq(i).children("input").eq(2).val();
        createKey.key4=$(".key-height").children("form").eq(i).children("input").eq(3).val();
        if((createKey.key1!="")&&(createKey.key2!="")&&(createKey.key3!="")&&(createKey.key4!="")){
            groupCreateKeys[i]=createKey;
        }
    }
    //console.log(groupCreateKeys);
    if(groupCreateKeys.length>0){
        $("#keywordAdd").off("click",addKeys);
        for(var i=0; i<groupCreateKeys.length; i++){
            //console.log(i);
            $.ajax({
                type: "POST",
                async: false,
                url: "http://192.168.0.43:8080/HelperManage/keyword/insertKeyword",
                //url:"test/groupKey.json",
                data: {
                    "city":groupCreateKeys[i].key1,
                    "keyword":groupCreateKeys[i].key2,
                    "startTime":groupCreateKeys[i].key3,
                    "endTime":groupCreateKeys[i].key4,
                    "roomid":$("#keyGroupName").attr("class")
                },
                dataType: "json",
                success: function () {
                    $.ajax({
                        type: "POST",
                        url: "http://192.168.0.43:8080/HelperManage/keyword/getKeywordByGid",
                        //url:"test/groupKey.json",
                        data: {"id":$("#keyGroupName").attr("class")},
                        dataType: "json",
                        success: function (data) {
                            $("#groupKeyList").html("");//清空groupKeyList内容
                            $.each(data.data,function (i, item) {
                                $("#groupKeyList").append(
                                    "<form class='form-inline keyModify' role='form'><input disabled='true' type='text' class='form-control marginL-3' value="+checkNull(item.city)+
                                    "><input disabled='true' type='text' class='form-control marginL-3' value="+checkNull(item.keyword)+
                                    "><input disabled='true' type='Date' class='form-control marginL-3' value="+checkNull(item.startTime)+
                                    "><input disabled='true' type='Date' class='form-control marginL-3' value="+checkNull(item.endTime)+
                                    "><button type='button' class='btn btn-default' name="+item.id+">修改</button></form>"
                                );
                            });
                        }
                    });
                }
            });
        }
        alert("关键字添加成功。");
        $("#keywordAdd").on("click",addKeys);
    }else {
        alert("添加关键字不能为空。");
    }
}
//群管理 修改绑定事件
function modifyGroup() {
    if($(this).text()=="修改"){
        $(this).text("保存");
        for(var i=4; i<11; i++) {
            var s = $(this).parent().parent().children().eq(i);
            var text=s.text();
            s.text("");
            s.append("<input type='text' class='form-control' value="+text+">");
        }
        $(".groupTable tbody tr td").has("input").css("padding","0");
    } else{
        for(var i=4; i<11; i++) {
            var s = $(this).parent().parent().children().eq(i);
            var text=s.find("input").val();
            s.remove("input").text(text);
        }
        $(".groupTable tbody tr td").css("padding","8px");
        var data=new Object();
        data.roomid=$(this).parent().parent().children().eq(0).text();
        data.area=$(this).parent().parent().children().eq(4).text();
        data.province=$(this).parent().parent().children().eq(5).text();
        data.city=$(this).parent().parent().children().eq(6).text();
        data.department=$(this).parent().parent().children().eq(7).text();
        data.subName=$(this).parent().parent().children().eq(8).text();
        data.channel=$(this).parent().parent().children().eq(9).text();
        data.alias=$(this).parent().parent().children().eq(10).text();
        //发送保存修改的请求
        console.log(data);
        //saveModify(data);
        $(this).text("修改");
    }

}
//发送保存修改的请求
function saveModify(data) {
    $.ajax({
        type: "POST",
        url: "",
        data: data,
        dataType: "json",
        });
}
//左侧导航栏开关
function switchNav() {
    if($(".sidebar-child").css("display")=="none"){
        $(".sidebar-child").css("display","block");
        $(".caret").removeClass("nav-open");
    }else{
        $(".sidebar-child").css("display","none");
        $(".caret").addClass("nav-open");
    }
}
//激活tab页
function chooseTab() {
    var lengthArray=activeTab.length;
    activeTab[lengthArray]=$(".nav-tabs .active");
    //console.log(activeTab);
    $(".active")
        .removeClass("active");
    $(".close").addClass("none-box");
    var liName=$(this).attr("name");
    $(this)
        .addClass("active")
        .find("span").filter(".close")
        .removeClass("none-box");

    $(".con-box").not(".none-box").addClass("none-box");
    $("div[id^="+liName+"]").removeClass("none-box");

    $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
    $("a[id^="+liName+"]").addClass("sidebar-click");
}
//关闭tab页
function closeTab() {
    if($(".nav-tabs li").length>1) {
        var lengthArray = activeTab.length;
        $(this).parent().parent().remove();
        $(".con-box").not(".none-box").addClass("none-box");
        var liName = activeTab[lengthArray - 1].attr("name");
        while($(".nav-tabs").has("li[name="+liName+"]").length==0){
            activeTab.splice(lengthArray - 1,1);
            lengthArray = activeTab.length;
            liName = activeTab[lengthArray - 1].attr("name");
        }
        $("div[id^=" + liName + "]").removeClass("none-box");
        activeTab[lengthArray - 1].addClass("active")
            .find(".close")
            .removeClass("none-box");
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
        $("a[id^=" + liName + "]").addClass("sidebar-click");
        activeTab.splice(lengthArray - 1, 1);
        //console.log(activeTab);
    }else{
        activeTab.splice(0,activeTab.length);
        $(".nav-tabs li").remove();
        $(".con-box").not(".none-box").addClass("none-box");
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
    }
}
//打开tab页
function openTab(event) {
    var lengthArray=activeTab.length;
    activeTab[lengthArray]=$(".nav-tabs .active");
    //console.log(activeTab);
    if(event.target.id=="smAssistant-nav"){
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
        $(this).addClass("sidebar-click");
        if($(".nav-tabs").find(".smAssistant-tab").length<=0){
            $(".active")
                .removeClass("active")
                .find(".close").addClass("none-box");
            $(".nav-tabs").append($tabs[0]);
            $(".smAssistant-tab span").on("click",closeTab);
        }else{
            $(".active")
                .removeClass("active")
                .find(".close")
                .addClass("none-box");
            $(".smAssistant-tab")
                .addClass("active")
                .find(".close")
                .removeClass("none-box");
        }
        $(".con-box").not(".none-box").addClass("none-box");
        $("div[id^='smAssistant']").removeClass("none-box");
    }
    if(event.target.id=="group-nav"){
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
        $(this).addClass("sidebar-click");
        if($(".nav-tabs").find(".group-tab").length<=0){
            $(".active")
                .removeClass("active")
                .find(".close").addClass("none-box");
            $(".nav-tabs").append($tabs[1]);
            initGroupTb();//初始化群管理
            $(".group-tab span").on("click",closeTab);
        }else{
            $(".active")
                .removeClass("active")
                .find(".close")
                .addClass("none-box");
            $(".group-tab")
                .addClass("active")
                .find(".close")
                .removeClass("none-box");
        }
        $(".con-box").not(".none-box").addClass("none-box");
        $("div[id^='group']").removeClass("none-box");
    }
    if(event.target.id=="message-nav"){
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
        $(this).addClass("sidebar-click");
        if($(".nav-tabs").find(".message-tab").length<=0){
            $(".active")
                .removeClass("active")
                .find(".close").addClass("none-box");
            $(".nav-tabs").append($tabs[2]);
            initMsgTb();//初始化消息管理
            $(".message-tab span").on("click",closeTab);
        }else{
            $(".active")
                .removeClass("active")
                .find(".close")
                .addClass("none-box");
            $(".message-tab")
                .addClass("active")
                .find(".close")
                .removeClass("none-box");
        }
        $(".con-box").addClass("none-box");
        $("div[id^='message']").removeClass("none-box");
    }
    if(event.target.id=="keyword-nav"){
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
        $(this).addClass("sidebar-click");
        if($(".nav-tabs").find(".keyword-tab").length<=0){
            $(".active")
                .removeClass("active")
                .find(".close").addClass("none-box");
            $(".nav-tabs").append($tabs[3]);
            initKeywordTb();//初始化关键字管理
            $(".keyword-tab span").on("click",closeTab);
        }else{
            $(".active")
                .removeClass("active")
                .find(".close")
                .addClass("none-box");
            $(".keyword-tab")
                .addClass("active")
                .find(".close")
                .removeClass("none-box");
        }
        $(".con-box").not(".none-box").addClass("none-box");
        $("div[id^='keyword']").removeClass("none-box");
    }
    if(event.target.id=="assistantGroup-nav"){
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
        $(this).addClass("sidebar-click");
        if($(".nav-tabs").find(".assistantGroup-tab").length<=0){
            $(".active")
                .removeClass("active")
                .find(".close").addClass("none-box");
            $(".nav-tabs").append($tabs[4]);
            initInGroupTb();
            $(".assistantGroup-tab span").on("click",closeTab);
        }else{
            $(".active")
                .removeClass("active")
                .find(".close")
                .addClass("none-box");
            $(".assistantGroup-tab")
                .addClass("active")
                .find(".close")
                .removeClass("none-box");
        }
        $(".con-box").not(".none-box").addClass("none-box");
        $("div[id^='assistantGroup']").removeClass("none-box");
    }
    if(event.target.id=="dataOut-nav"){
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
        $(this).addClass("sidebar-click");
        if($(".nav-tabs").find(".dataOut-tab").length<=0){
            $(".active")
                .removeClass("active")
                .find(".close").addClass("none-box");
            $(".nav-tabs").append($tabs[5]);
            $(".dataOut-tab span").on("click",closeTab);
        }else{
            $(".active")
                .removeClass("active")
                .find(".close")
                .addClass("none-box");
            $(".dataOut-tab")
                .addClass("active")
                .find(".close")
                .removeClass("none-box");
        }
        $(".con-box").not(".none-box").addClass("none-box");
        $("div[id^='dataOut']").removeClass("none-box");
    }

}

//关键字管理页面
//关键字列表的页码
var keywordPageNum=0;
//关键字页面取得表单信息
function getKeywordForm(pages) {
    var formData=new Object();
    formData.helper_id=$("#selectAssis-key").val();  //获取Select选择的Value;
    formData.keyword1=$("#keyword1").val();
    formData.keyword2=$("#keyword2").val();
    formData.edc=$("#edcTime").val();
    formData.page=pages;
    formData.pageSize=20;
   // console.log(formData);
    return formData;
}
//提交表单
function sendKeywordForm(formData) {
    $.ajax({
        type: 'POST',
        url: "http://192.168.0.43:8080/HelperManage/keyword/selectKeyword",
        data: formData,
        dataType: 'json',
        success: function(data){
            $("#keywordTbody").html("");//清空keywordTbody内容
            $.each(data.data, function (i, item) {
                $("#keywordTbody").append(
                    "<tr><td>" + item.id + "</td><td>" + checkNull(item.city) +
                    "</td><td>" + checkNull(item.groupKeyword) + "</td><td>" + checkNull(item.startTime)+"-"+checkNull(item.endTime) +
                    "</td><td>" + checkNull(item.groupId) + "</td><td>" + checkNull(item.isExists) + "</td></tr>");
            });
            $("#totalPages").text(data.data.total);
            if(data.data.total>0){
                $("#p1").text(keywordPageNum);
            }else {
                $("#p1").text(0);
            }
            $("#p2").text(data.data.count);
            alert("搜索成功了！");
        }
    });
}
//关键字管理，提交表单，处理返回结果
function keywordSearch() {
    keywordPageNum=1;
    var data= getKeywordForm(keywordPageNum);
    sendKeywordForm(data);

}
//初始化加载全部关键字信息的第一页
function initKeywordTb(){
    //初始化下拉菜单
    $.ajax({
        type: 'POST',
        url: "http://192.168.0.43:8080/HelperManage/helper/getAllHelper",
        data: "",
        dataType: 'json',
        success: function(data){
            $.each(data.data, function(i, item) {
                $("#selectAssis-key").append(
                "<option value="+item.robotid+">"+item.robotname+"</option>");
            });
            $("#selectAssis-key").selectpicker('refresh');
            $("#selectAssis-key option:first-child").attr("selected", true);
            //初始化关键字列表
            keywordSearch();
        }
    });

}
//关键字上一页
function prevKeyPage() {
    if(keywordPageNum>1) {
        keywordPageNum--;
        var data= getKeywordForm(keywordPageNum);
        sendKeywordForm(data);
        alert("上一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是第一页了哦！");
    }
}
//关键字下一页
function nextKeyPage() {
    if(keywordPageNum<parseInt($("#p2").text())) {
        keywordPageNum++;
        var data= getKeywordForm(keywordPageNum);
        sendKeywordForm(data);
        alert("下一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是最后一页了哦！");
    }
}

//消息管理页面
//消息列表的页码
var msgPageNum=0;
//取得消息搜索表单
function getMsgForm(pages) {
    var formData=new Object();
    formData.helper_id=$("#msgAssistant").val();  //获取Select选择的Value;
    formData.upMessage=$("#message").val();
    formData.username=$("#nickname").val();
    formData.status="";
    formData.page=msgPageNum;
    formData.pageSize=20;
    return formData;
    //console.log(pages);
}
//发送表单
function sendMsgForm(formData) {
    console.log(formData);
    $.ajax({
        type: 'POST',
        url: "http://192.168.0.43:8080/HelperManage/message/selectMessage",
        data: formData,
        dataType: 'json',
        success: function(data){
            $("#msgTbody").html("");//清空msgTbody内容
            $.each(data.data.messages, function(i, item) {
                var linkStatus="";
                // if(item.time!=undefined){
                //     linkStatus+="<span>发送群链接时间：</span>"+item.time+"<br>";
                // }
                // if(item.status!=undefined){
                //     linkStatus+="<span>发送群链接状态：</span>"+item.status+"<br>";
                // }
                // if(item.grpName!=undefined){
                //     linkStatus+="<span>发送群名：</span>"+item.grpName+"<br>";
                // }
                // if(item.keyGroup!=undefined){
                //     linkStatus+="<span>关键字匹配群：</span>"+item.keyGroup+"<br>";
                // }
                // if(item.remark!=undefined){
                //     linkStatus+="<span>备注：</span>"+item.remark;
                // }
                $("#msgTbody").append(
                    "<tr><td>"+$("#msgAssistant :selected").text()+"</td><td>"+item.upmessage+
                    "</td><td>"+item.messageprocessstatus+"</td><td>"+item.remindmessage+
                    "</td><td>"+item.username+"</td><td>"+item.messagestatus+
                    "</td><td>"+item.confirmstatus+"</td><td>"+item.messagingtime+
                    "</td><td>"+" "+"</td><td>"+linkStatus+"</td></tr>"
                );
            });
            $("#totalPages-msg").text(data.data.total);
            if(data.data.total>0){
                $("#p1-msg").text(msgPageNum);
            }else {
                $("#p1-msg").text(0);
            }
            $("#p2-msg").text(data.data.count);
            alert("搜索成功了！");
        }
    });
}
//消息管理，提交表单，处理返回结果
function msgSearch() {
    msgPageNum=1;
    var data=getMsgForm(msgPageNum);
    sendMsgForm(data);
}
//初始化加载全部消息管理信息的第一页
function initMsgTb(){
    //初始化下拉菜单
    $.ajax({
        type: 'POST',
        url: "http://192.168.0.43:8080/HelperManage/helper/getAllHelper",
        data: "",
        dataType: 'json',
        success: function(data){
            $.each(data.data, function(i, item) {
                $("#msgAssistant").append(
                    "<option value="+item.robotid+">"+item.robotname+"</option>");
            });
            $("#msgAssistant").selectpicker('refresh');
            $("#msgAssistant option:first-child").attr("selected", true);
            //初始化消息列表
            msgSearch();
        }
    });

}
//消息管理上一页
function prevMsgPage() {
    if(msgPageNum>1) {
        msgPageNum--;
        var data= getMsgForm(msgPageNum);
        sendMsgForm(data);
        alert("上一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是第一页了哦！");
    }

}
//消息管理下一页
function nextMsgPage() {
    if(msgPageNum<parseInt($("#p2-msg").text())) {
        msgPageNum++;
        var data= getMsgForm(msgPageNum);
        sendMsgForm(data);
        alert("下一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是最后一页了哦！");
    }
}


//群管理页面
//群列表的页码
var groupPageNum=0;
//群管理页面取得表单信息
function getGroupForm(pages) {
    var formData=new Object();
    formData.groupName=$("#groupName").val();
    formData.page=pages;
    formData.pageSize=20;
    //console.log(formData);
    return formData;
}
//提交表单
function sendGroupForm(formData) {
    $.ajax({
        type: 'POST',
        url: "http://192.168.0.43:8080/HelperManage/group/getGroup",
        //url:"test/groupList.json",
        data: formData,
        dataType: 'json',
        success: function(data){
            $("#groupTbody").html("");//清空groupTbody内容
            $.each(data.data.groups, function (i, item) {
                $("#groupTbody").append(
                "<tr><td>"+item.roomID+"</td><td>"+item.roomName+
                "</td><td>"+item.userCount+"</td><td>"+checkNull(item.area)+
                "</td><td>"+checkNull(item.province)+"</td><td>"+checkNull(item.city)+
                "</td><td>"+checkNull(item.department)+"</td><td>"+checkNull(item.subName)+
                "</td><td>"+checkNull(item.channel)+"</td><td>"+checkNull(item.alias)+
                "</td><td><a>修改</a>&nbsp;&nbsp;<a>关键字管理</a></td></tr>"
                );
            });
            $("#totalPages-group").text(data.data.total);
            if(data.data.total>0){
                $("#p1-group").text(groupPageNum);
            }else {
                $("#p1-group").text(0);
            }
            $("#p2-group").text(data.data.count);
            alert("搜索成功了！");
        }
    });
}
//群管理，提交表单，处理返回结果
function groupSearch() {
    groupPageNum=1;
    var data= getGroupForm(groupPageNum);
    sendGroupForm(data);

}
//查看全部
function viewAll() {
    groupPageNum=1;
    var data=new Object();
    data.groupName="";
    data.page=groupPageNum;
    data.pageSize=20;
    sendGroupForm(data);
}
//初始化加载全部群管理信息的第一页
function initGroupTb(){
    //初始化群管理列表
    groupSearch();
}
//群信息上一页
function prevGroupPage() {
    if(groupPageNum>1) {
        groupPageNum--;
        var data= getGroupForm(groupPageNum);
        sendGroupForm(data);
        alert("上一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是第一页了哦！");
    }
}
//群信息下一页
function nextGroupPage() {
    if(groupPageNum<parseInt($("#p2-group").text())) {
        groupPageNum++;
        var data= getGroupForm(groupPageNum);
        sendGroupForm(data);
        alert("下一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是最后一页了哦！");
    }
}
//群管理，关键字修改返回群管理
function backGroup() {
    $("#keyGroupName").attr("class","");
    $(".con-box").not(".none-box").addClass("none-box");
    $("div[id^='group']").removeClass("none-box");
    $("#keyModify").addClass("none-box");
}

//小助手所在群页面
//小助手所在群列表页码
var inGroupPageNum=0;
//小助手所在群页面取得表单信息
function getinGroupForm(pages) {
    var formData=new Object();
    formData.helper_id=$("#assis-group").val();  //获取Select选择的Value;
    formData.groupName=$("#groupSearch").val();
    formData.page=pages;
    formData.pageSize=20;
    //console.log(formData);
    return formData;
}
//提交表单
function sendInGroupForm(formData) {
    $.ajax({
        type: 'POST',
        url: "http://192.168.0.43:8080/HelperManage/group/getGroupByHelper",
        data: formData,
        dataType: 'json',
        success: function(data){
            $("#inGroupTbody").html("");//清空inGroupTbody内容
            $.each(data.data, function (i, item) {
                $("#inGroupTbody").append(
                    "<tr><td>" + item.robotName + "</td><td>" + item.status +
                    "</td><td>" + item.roomName + "</td><td>" + checkNull(item.enterTime) +
                    "</td><td>" + checkNull(item.roomCode) + "</td></tr>");
            });
            $("#totalPages-inGropu").text(data.data.total);
            if(data.data.total>0){
                $("#p1-inGropu").text(inGroupPageNum);
            }else {
                $("#p1-inGropu").text(0);
            }
            $("#p2-inGropu").text(data.data.count);
            alert("搜索成功了！");
        }
    });
}
//小助手所在群，提交表单，处理返回结果
function inGroupSearch() {
    inGroupPageNum=1;
    var data= getinGroupForm(inGroupPageNum);
    sendInGroupForm(data);

}
//初始化加载小助手所在群信息的第一页
function initInGroupTb(){
    //初始化下拉菜单
    $.ajax({
        type: 'POST',
        url: "http://192.168.0.43:8080/HelperManage/helper/getAllHelper",
        data: "",
        dataType: 'json',
        success: function(data){
            $.each(data.data, function(i, item) {
                $("#assis-group").append(
                    "<option value="+item.robotid+">"+item.robotname+"</option>");
            });
            $("#assis-group").selectpicker('refresh');
            $("#assis-group option:first-child").attr("selected", true);
            //初始化小助手所在群列表
            inGroupSearch();
        }
    });
}
//小助手所在群上一页
function prevInGropuPage() {
    if(inGroupPageNum>1) {
        inGroupPageNum--;
        var data= getinGroupForm(inGroupPageNum);
        sendInGroupForm(data);
        alert("上一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是第一页了哦！");
    }
}
//小助手所在群一页
function nextInGropuPage() {
    if(inGroupPageNum<parseInt($("#p2-inGropu").text())) {
        inGroupPageNum++;
        var data= getinGroupForm(inGroupPageNum);
        sendInGroupForm(data);
        alert("下一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是最后一页了哦！");
    }
}
//非空验证
function notNull() {
   if(this.value==""){
       alert("这里的输入不能为空哦。");
   }
}

//数据导出页面
//小助手加好友发链接情况报表
function reportsLink() {
    $.ajax({
        type: 'POST',
        url: "",
        data: {
            "time":$("#outStartTime1").val()
        },
        success: function(data){
          alert("小助手加好友发链接情况报表导出成功。");
        }
    });
}
//小助手入群时间明细报表
function reportsTime() {
    $.ajax({
        type: 'POST',
        url: "",
        data: {
            "time":$("#outStartTime2").val()
        },
        success: function(data){
            alert("小助手入群时间明细报表导出成功。");
        }
    });
}
//退出
function dropOut() {
    // $.ajax({
    //     type: "GET",
    //     url: "",
    //     success: function () {
    //         window.location.href="login.html";
    //     }
    // });
    window.location.href="login.html";
}
//处理数据为null的表格显示问题
function checkNull(arg) {
    if(arg==null){
        return "";
    }else {
        return arg;
    }
}